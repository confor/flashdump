/* jshint esversion: 8 */

const fs = require('fs');
const got = require('got');
const cheerio = require('cheerio');

const CATEGORIES = 'action,puzzle,arcade,fighting,rpg,sport'.split(',');
const EXTRACTED = [];

function get_text_from_domnode(node) {
	let text = '';

	if (node.type === 'text')
		text += node.data;

	if (node.children) {
		for (let i = 0; i < node.children.length; i++) {
			let child = node.children[i];

			text += get_text_from_domnode(child);
		}
	}

	return text;
}

async function scrape(category) {
	var html = await got('https://www.crazymonkeygames.com/tag/' + category + '?q=name'),
		qs = cheerio.load(html.body);

	var list = qs('#main-content .search-results li');

	for (let i = 0; i < list.length; i++) {
		let item = list.get(i);

		if ('class' in item.attribs && item.attribs['class'] === 'ad')
			continue;

		var page = '',
			title = '',
			text = '',
			icon = '';

		for (let j = 0; j < item.children.length; j++) {
			let children = item.children[j];

			// <h3><a href="/game.html">Title game here</a></h3>
			if (children.tagName === 'h3' && children.firstChild && children.firstChild.tagName === 'a') {
				let link = children.firstChild;

				title = get_text_from_domnode(link);
				page = link.attribs.href;
			} else if (children.tagName === 'p') {
				text = get_text_from_domnode(children);
			}
		}

		let image = qs(item).find('img');
		if (image.length === 1 && image[0].attribs)
			icon = image[0].attribs.src;

		if (page && title && text) {
			EXTRACTED.push({
				category,
				title,
				text,
				icon,
				page
			});
		}
	}
}

async function find_swf(game) {
	var html = await got(game.page),
		qs = cheerio.load(html.body);

	var embed = qs('object#gameObject embed[src]');

	if (embed.length === 1)
		game.file = embed[0].attribs.src;
	else
		console.log('somethings fucked up', game);
}

(async() => {
	for (let i = 0; i < CATEGORIES.length; i++) {
		console.log('scraping ' + CATEGORIES[i]);
		await scrape(CATEGORIES[i]);
	}

	for (let j = 0; j < EXTRACTED.length; j++) {
		let game = EXTRACTED[j];
		await find_swf(game);
	}

	fs.writeFileSync('dump-' + Date.now() + '.json', JSON.stringify(EXTRACTED));
	console.log('scraped ' + EXTRACTED.length + ' games');
})();
