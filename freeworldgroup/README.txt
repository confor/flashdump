freeworldgroup dump script
==========================
script to scrape freeworldgroup.com and dump all their flash games.

downloading the dump
--------------------
i uploaded the 5000+ games to archive.org
https://archive.org/details/arcade_202002

contains, split by category:
- 886 adventure
- 1469 arcade
- 80 board
- 366 brain
- 36 cards
- 1069 puzzle
- 870 quick/casual
- 428 sports/racing
- 745 strategy

19gb (20966 mb) in total.

disclaimer
----------
i do not have permission of fwg to download and distribute their games.
they don't make games, however. i suspect they didn't have permission to
put others games on their website either. let me know if this is a problem?

the scraped data is available on this repo and there is no need to run
the scripts. game pages are on `data/` and game file locations are under
`games/`, separated by category.

the code quality is super bad. theres no error handling, and the code is
generally a huge mess.

how to use
----------
after manually putting every game from the category lists in a text file,
run `parse.js`. it'll try to open the .txt files in `data/` and will scrape
every given page and output the game files into a tab separated file on
`output.csv` with the following format:
    title  status  category  url
where:
- title: the title of the game
- status: "ok" if the game was scraped, "ignored", "skip" and "fail" on errors.
- category: the category it belongs to. just the `.txt` name, really.
- url: the url where the .swf or .dcr or .jar can be directly downloaded.

one can then use `download.sh` to download every `url` in `output.csv`.
it will also throw some metadata in `info.txt` next to every game.
the folder structure will look like this:
    + downloads/
    |--+ Game 1/
    |  |-- gamefile.swf
    |  \-- info.txt
    +--- Game 2/
    \--- Game 3/...

pay attention to the last line of `download.sh`, the input filename is there.

how it works
------------
given a text file which contains a json array of fwg pages (this is manually
scraped, like, by a person), download every page's html and try to discover
where the .swf file is. this is done by `parse.js` and `extract.js` by using
css selectors i carefully crafted after fiddling with their website for a few
hours.

their website has about 19 different ways of embedding an .swf file, so the
`extract.js` script deals with that. this project skips downloading html5,
unity3d, java applets and other uncommon formats.

i used:
- gnu bash 4.4.23
- wget 1.20
- node 10.16
- 7zip 19.00 (needed zip64 for >4 GiB archives)

and these node modules:
- cheerio 1.0.0-rc3
- got 10.5.7

scraping the lists for games
----------------------------
i didn't write any software for it, just manually scraped each category in my
browser and copypasted the output on a .txt file

consider the "arcade games" category under /gamesarcade.htm
- scroll down until you see a big list of games. the list is divided in chunks
- right click, inspect, find the element that contains the entire list.
- run the following javascript to extract an array in the form of [name, url]
      Array.prototype.map.call($0.querySelectorAll('a[href]'),
                               a => JSON.stringify([a.textContent, a.href]))
- copy and paste it into a text file named `arcade.txt`
- repeat for next page, then next category. theres 3 to 6 pages per category,
  and there are 10 categories.

why
---
wanted to help the flashpoint project
https://bluemaxima.org/flashpoint/
