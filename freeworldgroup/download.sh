#!/bin/bash
# tested in gnu bash 4.4.23

mkdir -p downloads/
timestamp="$(date +'%Y-%m-%d %R')"

#limit=4 # debug

while IFS= read -r line; do
	while IFS=$'\t' read -r -a data; do
		status="${data[1]}"
		
		# skip games that were skipped or errored out
		if [ "$status" != "ok" ]; then
			continue
		fi
		
		name="$(sed 's/[^a-zA-Z0-9\-]/_/g' <<< "${data[0]}")"
		category="${data[2]}"
		url="${data[3]}"
		echo "process $name"
		mkdir -p "downloads/$name"
		
		# some metadata
		# TODO: use just one command
		# if this script runs again, it'll append instead of overwriting
		echo "($timestamp)" >> "downloads/$name/info.txt"
		echo "title: ${data[0]}" >> "downloads/$name/info.txt"
		echo "category: ${data[2]}" >> "downloads/$name/info.txt"
		echo "url: $url" >> "downloads/$name/info.txt"
		
		# if this script is ran multiple times, wget will keep adding
		# files instead of overwriting. i think this is fine.
		if ! wget --quiet --directory-prefix "downloads/$name/" "$url"; then
			echo "warning: something failed while download this game"
			continue
		fi
		
	done <<< "$line"
	
	#if [ "$limit" -eq 0 ]; then
	#	exit 0
	#fi
	#limit=$((limit-1))
	
done < games/arcade.tsv