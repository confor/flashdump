/*
 *  ____  ______ _    _  ____  _      _____  
 * |  _ \|  ____| |  | |/ __ \| |    |  __ \ 
 * | |_) | |__  | |__| | |  | | |    | |  | |
 * |  _ <|  __| |  __  | |  | | |    | |  | |
 * | |_) | |____| |  | | |__| | |____| |__| |
 * |____/|______|_|  |_|\____/|______|_____/ 
 *     the worst function names to ever be
 * 
 * 
 * returned obj: {
 *    (string) location: relative url where you can find the game
 * }
 */

function scream() {
	console.log('AA'.repeat(80));
}

function exists(query) {
	return query && query.get().length === 1;
}

// context: page is /games3/spaceinvaders/index.html
// game is in <object><embed src="spaceinvaders.swf"></object> inside
// a huge, never-ending mess of tables
function extract_games3_swf_embedded(selector) {
	var game = selector('table table table object embed[src]');

	if (!exists(game))
		return false;

	return {
		location: game.attr('src')
	};
}

// context: page is /games6/gameindex/spaceinvaders.htm
// game is in <object><embed src="spaceinvaders.swf"></object>
function extract_games6_swf_embedded(selector) {
	var game = selector('.homeBody object embed[src]');

	if (!exists(game))
		return false;

	return {
		location: game.attr('src')
	};
}

// page: /games8/gameindex/spaceinvaders.htm
// game is in <embed src="spaceinvaders.swf">
function extract_games8_swf_embedded(sel) {
	var game;
	
	if (exists(sel('#precontent_ad object#intergi_wrapper_player')))
		game = sel('#content_wrapper embed[src]');
	else
		game = sel('.homeBody embed[src]');
	
	if (!exists(game))
		return false;
	
	return {
		location: game.attr('src')
	}
}

// page: /games8/gameindex/spaceinvaders.htm
// game is in <script>, will load after ad
function extract_games8_swf_in_script_behind_ad(sel) {
	var scripts = sel('script').get();

	if (scripts.length < 1)
		return false;

	let text;

	// we have to find which <script> contains the
	// google ad that loads the game
	for (let i = 0; i < scripts.length; i++) {
		let script = scripts[i];

		if (script.childNodes.length !== 1)
			continue;

		for (let j = 0; j < script.childNodes.length; j++) {
			let child = script.childNodes[j];
			
			if (child.type !== 'text')
				continue;
			
			// we found the javascript with the game loader!
			if (child.data.includes('iframe_ads_loader.swf')) {
				text = '' + child.data;
				break;
			}
		}
		
		// skip other <script>s
		if (text)
			break;
	}

	// didnt find the game?, suffer
	if (!text)
		return false;

	text = text.split('\n');
	
	var location;
	
	// find some line followed by another
	for (let k = 0; k < text.length; k++) {
		let line = text[k];
		if (text[k].includes('showFwgLoader') && text[k+1].includes('swf')) {
			// found the game! url is surrounded by quotes, whitespace or commas
			location = text[k+1].replace(/^[" ]+/g, '').replace(/[",]+$/, '');
			break;
		}
	}

	return {
		location
	};
}

// page: /games9/gameindex/spaceinvaders.htm
// game is in <object><param name="movie" value="spaceinvaders.swf"></object>
function extract_games9_swf_in_param(sel) { // TODO rename, isn't in <param>
	var obj = sel('.homeBody object#FlashID object[data]');
	
	if (!exists(obj))
		return false;
	
	return {
		location: obj.attr('data')
	}
}

// page: /fun-games/gameindex/spaceinvaders.htm
// game is in <embed id=contentSwf src="spaceinvaders.swf">
function extract_fungames_swf_embedded(sel) {
	var game = sel('.homeBody embed#contentSwf[src]');

	if (!exists(game))
		return false;

	return {
		location: game.attr('value')
	}
}

// page: /games2/gameindex/spaceinvaders.htm
// game is in <applet archive="spaceinvaders.jar">
function extract_games2_jar(sel) {
	var java = sel('.homeBody applet[archive]');

	if (!exists(java))
		return false;

	return {
		location: java.attr('archive')
	}
}

function extractor(selector, type) {
	switch (type) {
		case 'games6, swf in embed in object':
		case 'games5, swf in embed in object':
		case 'games4, dcr/swf in embed in object':
		case 'games3, swf in embed in object':
		case 'games2, swf in embed in object':
		case 'games, swf in embed in object':
			return extract_games6_swf_embedded(selector);
		
		case 'games3, swf hidden somewhere in tables':
			return extract_games3_swf_embedded(selector);
		
		case 'games8, swf in embed':
		case 'games9, swf in embed':
			return extract_games8_swf_embedded(selector);
		
		case 'games8, swf in object in object':
		case 'games9, swf in param in object':
			return extract_games9_swf_in_param(selector);
			
		case 'fun-games, swf in embed':
			return extract_fungames_swf_embedded(selector);
		
		case 'games2, swf behind javascript ad':
		case 'games6, swf in javascript behind ad':
		case 'games8, swf in script behind ad':
		case 'games9, swf in script behind ad':
			return extract_games8_swf_in_script_behind_ad(selector);
		
		case 'games2, jar':
			return extract_games2_jar(selector);
	}
}

module.exports = exports = {
	extractor
};