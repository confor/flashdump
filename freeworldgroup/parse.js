const fs = require('fs');
const url = require('url');
const readline = require('readline');
const { pipeline } = require('stream');

// 3rd party
const got = require('got');
//const mkdirp = require('mkdirp');
const cheerio = require('cheerio');

const { extractor } = require('./extractor.js');

const DATA_DIR = 'data/';
const DATA_FILE = 'output.csv';

var FILES = [
	'adventure.txt',
	'arcade.txt',
	'board.txt',
	'brain.txt',
	'cards.txt',
	'multiplayer.txt',
	'puzzle.txt',
	'quick.txt',
	'sports and racing.txt',
	'strategy.txt'
];

const data = FILES.map(filename => {
	let content = JSON.parse(fs.readFileSync(DATA_DIR + filename, { encoding: 'utf8' }));

	return {
		name: filename.substring(0, filename.indexOf('.')),
		content//: content.splice(0, 4)
	};
});

async function hack() {
	for (let i = 0; i < data.length; i++) {
		var category = data[i];
		console.log('-----------------\n - ' + category.name + '\n-----------------');
		
		for (let j = 0; j < category.content.length; j++) {
			let game = category.content[j],
				name = game[0],
				page = game[1];
			
			await download(name, page, category.name);
		}
	}
	
	clear_buffer();
}

touch_data_output();
hack();

async function download_swf(filename, url) {
	await pipeline(
		got.stream(url),
		fs.createWriteStream(filename)
	);
}

/*function escape_name(unsafe) {
	return unsafe.replace(/[^a-z0-9 \-\.]/gi, '_');
}

function ensure_download_dir(category) {
	mkdirp('games/' + category);
}*/

var BUFFER = '';
function append_gamefile(name, page, category, status, file) {
	// name = game name
	// page = the html
	// category = tag, like arcade or racing
	// file = where to find the .swf, .dcr or .jar
	
	debugger;
	if (typeof file === 'string') {
		if (file.startsWith('//'))
			file = 'http:' + file;
		else if (file.startsWith('http'))
			file = file; // ??!?
		else
			file = url.resolve(page, file);
	}

	name = name.replace(/[\t\n]/g, '_');
	category = category.replace(/[^a-z]/g, '_');

	BUFFER += [name, status, category, file].join('\t') + '\n';

	if (BUFFER.length > 1024) {
		// don't catch errors
		// if we can't save the games then whats the point, just crash
		fs.appendFileSync(DATA_DIR + DATA_FILE, BUFFER);
		BUFFER = '';
	}
}

function touch_data_output() {
	fs.writeFileSync(DATA_DIR + DATA_FILE, '');
}

function clear_buffer() {
	fs.appendFileSync(DATA_DIR + DATA_FILE, BUFFER);
	BUFFER = '';
}

async function download(name, page, category, type='unknown') {
	console.log('\ndownloading ' + name + ' at ' + page);
	var html = await got(page);
	var selector = cheerio.load(html.body);

	if (type === 'unknown' || type === 'redirect') {
		/*
		 * warning to those who dare enter this shithole:
		 * i dont know what freeworldgroup was thinking, but they managed
		 * to insert a .swf in like 8903499999990e+7 different ways.
		 *
		 * this code figures out where the .swf is.
		 */
		if (page.includes('/games9/gameindex/')) {
			// game in <embed src="game.swf">
			attempt = extractor(selector, 'games9, swf in embed');
			if (attempt === false) {
				// game in <object><param value="game.swf"></object>
				attempt = extractor(selector, 'games9, swf in param in object');
				
				if (attempt === false) {
					// could be inside an ad?
					attempt = extractor(selector, 'games9, swf in script behind ad');
					
					if (attempt === false) {
						append_gamefile(name, page, category, 'fail', 0);
						console.log('FAIL ' + name + ', probably third-party iframe');
						return;
					}
					
					append_gamefile(name, page, category, 'ok', attempt.location);
					console.log('SUCcEsS (in ad) ' + name);
					return;
				}
				
				append_gamefile(name, page, category, 'ok', attempt.location);
				console.log('SUCCESS with param: ' + name);
				return;
			}
			
			append_gamefile(name, page, category, 'ok', attempt.location);
			console.log('success with embed: ' + name);
			return;
		}
		
		else if (page.includes('/games/') && page.includes('/index')) {
			var attempt;
			
			// game could be flash,
			attempt = extractor(selector, 'games, swf in embed in object');
			
			if (attempt === false) {
				// or it could be shockwave
				// or java...
				append_gamefile(name, page, category, 'fail', 1);
				console.log('SKIP: ' + name + ' can be java or other plugin');
				return;
			}
			
			append_gamefile(name, page, category, 'ok', attempt.location);
			console.log('SUCCess (somehow): ' + name);
			return;
		}
		
		else if (page.includes('/games8/gameindex/')) {
			var attempt;
			
			//lol at this chunk of code
			if (type !== 'redirect') {
				// see if its a redirect
				let test = selector('meta[http-equiv]');
				if (test.get().length === 1 && test.attr('http-equiv') === 'refresh') {
					let metadata = test.attr('content'),
						redirect = metadata.substring(metadata.indexOf('URL=') + 'URL='.length);
					console.log('found redirect');
					download(name, redirect, category, 'redirect');
				}
			}
			
			// game is in <embed src>
			attempt = extractor(selector, 'games8, swf in embed');
			if (attempt === false) {
				// game could be behind an ad script
				attempt = extractor(selector, 'games8, swf in script behind ad');
				if (attempt === false) {
					// game could be inside some internet explorer workaround
					attempt = extractor(selector, 'games8, swf in object in object');
					if (attempt === false) {
						append_gamefile(name, page, category, 'fail', 2);
						console.log('FAIL: ' + name);
						return;
					}

					append_gamefile(name, page, category, 'ok', attempt.location);
					console.log('succ got it ' + name);
					return;
				}

				append_gamefile(name, page, category, 'ok', attempt.location);
				console.log('success (in ad): ' + name);
				return;
			}
			
			append_gamefile(name, page, category, 'ok', attempt.location);
			console.log('success (embed): ' + name);
			return;
		}
		
		else if (page.includes('/games2/gameindex/')) {
			var attempt;
			
			// game is in <object><embed src="game.swf"></object>
			// also in <param name="movie" value="game.swf">
			attempt = extractor(selector, 'games2, swf in embed in object');
			if (attempt === false) {
				// could be <applet archive="game.jar">
				attempt = extractor(selector, 'games2, jar');
				
				if (attempt === false) {
					// could be behind an ad wall
					// note: the javascript inside <script> is different from
					// others, this is the reason the code over at extractor.js
					// tries so hard to find some line with ".swf"
					
					attempt = extractor(selector, 'games2, swf behind javascript ad');
					if (attempt === false) {
						append_gamefile(name, page, category, 'fail', 3);
						console.log('FAIL big, have no idea: ' + name);
						return;
					}

					append_gamefile(name, page, category, 'ok', attempt.location);
					console.log('SUCCESS!!!!!! ' + name);
					return;
				}

				append_gamefile(name, page, category, 'ok', attempt.location);
				console.log('success with java!! ' + name);
				return;
			}

			append_gamefile(name, page, category, 'ok', attempt.location);
			console.log('SUCCESS: ' + name);
			return;
		}
		
		else if (page.includes('/fun-games/gameindex/')) {
			var attempt;
			
			// game is in <embed id="contentSwf" src="game.swf">
			attempt = extractor(selector, 'fun-games, swf in embed');
			if (attempt === false) {
				// if its in /fun-games/ but its NOT an swf,
				// its a fucking unity web player game.
				// can you believe this?
				append_gamefile(name, page, category, 'fail', 4);
				console.log('failed: ' + name + ', might be unity3d');
				return;
			}
			
			append_gamefile(name, page, category, 'ok', attempt.location);
			console.log('SUCCESs ' + name);
			return;
		}
		
		else if (page.includes('/games3/gameindex/') ||
				( page.includes('/games3/') && page.includes('/index') )
			) {
			var attempt;
			
			// game could be <object><embed src="game.swf">
			attempt = extractor(selector, 'games3, swf in embed in object');
			if (attempt === false) {
				// if its /games3/ and not /gameindex/ it could be
				// an iframe hidden behind an ad. the iframe sometimes
				// contains a <embed .swf> with completely random,
				// arbitrary paths, or the html is custom in such a way
				// that it doesn't match any other selector in extractor.js
				// so far, only "bump copter 2" has custom html.

				attempt = extractor(selector, 'games3, swf hidden somewhere in tables');
				if (attempt === false) {
					append_gamefile(name, page, category, 'fail', 5);
					console.log('FAILED COMPLETELY with ' + name);
					return;
				}

				append_gamefile(name, page, category, 'ok', attempt.location);
				console.log('success, somehow: ' + name);
				return;
			}
			
			append_gamefile(name, page, category, 'ok', attempt.location);
			console.log('SUCCESS: ' + name);
			return;
		}
		
		else if (page.includes('/games4/gameindex/') ||
			(page.includes('/games4/') && page.includes('/index'))) {
			var attempt;
			
			// game is in <object><embed src="game.swf">
			attempt = extractor(selector, 'games4, dcr/swf in embed in object');
			if (attempt === false) {
				append_gamefile(name, page, category, 'fail', 6);
				console.log('fail: ' + name);
				return;
			}
			
			append_gamefile(name, page, category, 'ok', attempt.location);
			console.log('success dcr/swf ' + name);
			return;
		}
		
		else if (page.includes('/games5/gameindex/')) {
			var attempt;
			
			// game is in <object><embed src="game.swf">
			attempt = extractor(selector, 'games5, swf in embed in object');
			if (attempt === false) {
				// could be <iframe> in the same website
				// not worth parsing, iframes have wildly different html
				append_gamefile(name, page, category, 'fail', 7);
				console.log('fail: ' + name);
				return;
			}
			
			append_gamefile(name, page, category, 'ok', attempt.location);
			console.log('success yeahhhh ' + name);
			return;
		}
		
		else if (page.includes('/games6/gameindex/')) {
			var attempt;
			
			// first attempt: swf in html
			attempt = extractor(selector, 'games6, swf in embed in object');
			if (attempt === false) {
				attempt = extractor(selector, 'games6, swf in javascript behind ad');
				
				if (attempt === false) {
					// might be an iframe to a third party.
					// also could be <embed src="http://thirdparty.com/game.swf">
					// but positioned in some weird spot in the html
					append_gamefile(name, page, category, 'fail', 8);
					console.log('failed to find games6: ' + name);
					return;
				}
			}
			
			append_gamefile(name, page, category, 'ok', attempt.location);
			console.log('success: ' + name + ' via games6 swf');
			return;

			//var location = url.resolve(page, attempt.location),
			//	filename = escape_name(name);

			//console.log(filename, location);
			//ensure_download_dir(category);
			//console.log('actually downloading ' + filename);
			///*var downloader = download_swf('games/' + category + '/' + filename + '.swf', location); // this code is all over the place i'm sorry lmao
			//downloader.catch(error => {
			//	console.log('error: ' + name + ' dl failed');
			//});
			//await downloader;*/
		}

		else if (page.includes('/g/i/')) {
			append_gamefile(name, page, category, 'skip', 10);
			console.log('skip ' + name + ': its an html5 game, or an iframe to a third party');
			return;
		}
		
		else {
			append_gamefile(name, page, category, 'ignored', 11);
			console.log('WARNING: unhandled ' + name);
		}
	}
	
	/*else if (type == 'game inside iframe') {
		// TODO load iframe and extract it
		var embed = selector('embed#contentSwf').get();
		if (embed.length === 0) {
			console.log('failure: game is not .swf');
			return;
		}
	}*/
}