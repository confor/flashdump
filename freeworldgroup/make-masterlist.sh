#!/bin/bash

masterlist="games.txt"
declare -a categories=("adventure" "arcade" "board" "brain" "cards" "puzzle" "quick" "sportsandracing" "strategy")

touch "$masterlist"

for category in "${categories[@]}"; do
	while IFS= read -r line; do
		while IFS=$'\t' read -r -a data; do
			# skip games that were skipped or errored out
			if [ "${data[1]}" != "ok" ]; then
				continue
			fi
			
			name="${data[0]}"
			category="${data[2]}"
			echo "($category) $name" >> "$masterlist"
		done <<< "$line"
	done < "games/$category.tsv"
done
